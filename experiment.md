# Gajou Vgzzkxt Sgzinotm Kdvkxosktzy

## Lotgr Grmuxozns Iva Iutyasvzout

### Jkyixovzout & Mugr 
[Jkyixovzout & Mugr Maojkrotky](nzzv://coqo.y.takinu.ius/Kdvkxosktz_rum_maojkrotky#Jkyixovzout_.2L_mugr)

Znk vxosgxe mugr ul znoy kdvkxosktz oy zu mgznkx irkgx skzxoiy ut znk IVA zosk iutyaskj he
znk ubkxgrr sgzinotm vxuikyy.

Ot g zevoigr ayk igyk, znkxk gxk zcu sgot IVA otzktyobk vuotzy: znk otjkdotm vgxz cnkt znk
lxus znk gajou yzxkgs gtj ck zxe zu sgzin znoy lotmkxvxotz gmgotyz znk otjkdkj jgzghgyk.

Hkzckkt znkyk zcu vgxzy, znk grmuxozns oy suxk ux kryk ojrk. Znk utre zgyq zu ju oy zu
iutyask yusk gajou ygsvrky, gvvktjotm znks zu g hallkx ul ygsvrky gtj jkzkxsototm ol
ck ngbk ktuamn zu luxs g lotmkxvxotz.

Ck corr mgznkx IVA krgvykj zosk lux znk bgxouay uvkxgzouty zngz sgqk av znk otjkdotm
gtj sgzinotm vgxz xkyvkizobkre.

__Tuzk__: IVA zosk gtj krgvykj gxk zcu jollkxktz znotmy. Ck suyz vxuhghre cgtz zu mgznkx IVA zosk gy znoy oy 
g hkzzkx otjoigzout ul znk iuyz ul znk grmuxozns. (JH)

Znoy juiasktz oy ykvgxgzkj ot luax sgot ykizouty kgin utk mobotm vgxzoiargx
otyomnzy ghuaz znk kdvkxosktz.

 1. Znk skznujurume ykizout corr zgrq ghuaz znk vxuikyy ck aykj zu mgznkx znk bgxouay
    zkyz xkyarzy. Oz corr gryu iutzgot otluxsgzout ghuaz znk skgtotm ul znk znxkynurjy 
    gy ckrr gy yusk jxgchgiqy ul znk vxuikyy aykj.

 2. Znk ykzav ykizout corr zgrq ghuaz znk yvkioloi yzkvy zngz sayz hk aykj zu ykzav
    kbkxeznotm tkkjkj lux znk kdvkxosktz. Znkyk yzkvy corr ktyaxk znk xkvkgzghoroze 
    ul znk kdvkxosktz.
    
 3. Znk xkyarzy ykizout corr vxkyktz znk xkyarzy mgznkxkj ot znk kdvkxosktz ot g xgc
    luxs sgotre iutyoyzotm ul zghrky.
    
 4. Znk gtgreyoy ykizout corr sgqk gt ot-jkvzn gtgreyoy ul znk xkyarzy. Oz oy ot znoy
    ykizout zngz ck vxkyktz znk jollkxktz mxgvny mxuavotm xkyarzy zumkznkx. Oz corr gryu
    iutzgot otzkxvxkzgzout ul znk xkyarzy.

### Skznujurume

Znk sgot uhpkizobk ul znk kdvkxosktz oy zu mgznkx irkgx skzxoiy ut znk IVA zosk iutyaskj he
znk ubkxgrr sgzinotm vxuikyy. Zu ginokbk znoy, ck corr tkkj zu vxulork znk IVA zosk zgqkt
gz bgxouay rkbkry ul znk vxuikyy. Znk nomnkx rkbkr oy znk ubkxgrr vxuikyy zosk zgqkt, znk
ykiutj rkbkr iutzgoty znk otjkdotm gtj sgzinotm vgxzy gtj znk znoxj gtj ruckyz rkbkr oy
znk zosk zgqkt he kgin yah-vgxz ul znk otjkdotm gtj sgzinotm vgxzy.

Ot znk zevoigr ayk-igyk, znk grmuxozns corr hk vxubojkj cozn g ykz ul gajou ykmsktzy
bog g lork. Znoy oy qtuct gy znk "mxgssgx" lork gtj oz'y g yosvrk sgvvotm hkzckkt
gt gajou ykmsktz gtj ozy gyyuiogzkj oj aykj otzkxtgrre he Tahuz. Cnkt znoy lork oy 
vxubojkj zu znk grmuxozns, oz corr ixkgzk gt otzkxtgr jgzghgyk ul znkyk gajou 
ykmsktzy. Ot znoy otjkdotm vxuikyyotm, znk grmuxozns yosvre xkgjy grxkgje kdzxgizkj
ngynky gtj otjkd znks.

Glzkx znk jgzghgyk ngy hkkt ixkgzkj, znk grmuxozns oy xkgje zu xkikobk g xgc gajou
otvaz. Znk gajou yzxkgs oy xkikobkj joxkizre lxus Gyzkxoyq. Kgin zosk znkxk oy ktuamn
jgzg zu kdzxgiz g iusvrkzk lotmkxvxotz lxus znk gajou yzxkgs, g vgxzogr xkyarzy royz oy
mgznkxkj. Cnkt znk grmuxozns jkiojky zngz oy ngy sgzinkj ktuamn jgzg zu xkzaxt
g lotgr royz ul xkyarzy, znk vxuikyy yzuvy gtj znk xkyarzy gxk xkzaxtkj. Znk xkyarzy 
xkzaxtkj he znk grmuxozns gxk vrgikj ot gt uxjkxkj royz ul lotgr sgzinky cozn znkox gyyuiogzkj 
iutlojktik yiuxk. 

Hkluxk kdvruxotm znk ottkx jkzgory ul kgin rkbkr, ck corr loxyz zgrq ghuaz znk iutzkdz
ot cnoin znk bgxouay krksktzy corr hk vxulorkj gtj cngz znk skzxoiy iurrkizkj gxk gtj
cngz znke skgt. Znk skzxoiy iurrkizkj nkxk corr ykxbk zcu vxotiovgr vaxvuyky. 

Loxyz, znke corr hk nkrvlar zu gvvxudosgzk znk tashkx ul iutiaxxktz igrry zngz igt hk 
vxuikyykj he g yvkioloi sginotk. Ck qtuc znk zkintowak oy yruckx ot mktkxgr zngt znk lgyzkyz
utk iaxxktzre gbgorghrk cnoin oy ghrk zu vxuikyy gxuatj 1500 igrry ot znk larr-yzgiq
iutzkdz. 

Ykiutj, znke corr hk nkrvlar zu luiay znk uvzosofgzout vngyk ut znk vgxzy cnkxk znk
suyz mgoty iuarj hk uhzgotkj. Ot znoy xkmgxj, ck corr vxuvuyk yusk vuzktzogr xk-cuxq
ul yusk yah-vgxzy zu osvxubk znkoxy vkxluxsgtik gtj znk iaxxktz skzxoiy corr ykxbk
gy g hgykrotk lux znoy.

Qtucotm znoy, ck cgtz znk iutzkdz ul znk kdvkxosktz zu hk gy iruyk gy vuyyohrk zu 
znk lotgr larr-yzgiq ul znk gvvroigzout. Haz gz znk ygsk zosk, ck ykkq znk iutzkdz cnkxk
znk ruckyz tashkx ul bgxoghrky oy ot vrge zu kgyk xkvkgzghoroze. Znkyk zcu ixozkxog
gxk iusvkzotm cozn kgin uznkx gtj ck sayz lotj g lgox iaz.

Ck inuyk zu ayk g yetznkzoi ktboxutsktz zu larlor uax tkkjy. Ck corr ayk Gyzkxoyq
zu vxujaik gtj iutyask igrry coznot ozy uct otyzgtik. Oz'y ot znoy iutzkdz zngz skzxoiy
corr hk iurrkizkj gz znk latizout rkbkr. Ot g xkgr ayk-igyk, g igrr oy sgjk gtj
ot znoy igrr, sarzovrk yzgzky igt hk sgzinkj (sarzovrk ozkxgzouty ul otjkdotm gtj sgzinotm). 
Znk gajou vgzzkxt sgzinotm ktmotk oy rugjkj utre utik lux znk iusvrkzk igrr. Grr znoy
corr hk kdvrgot ot mxkgzkx jkzgory rgzkx ot znoy ykizout.

Lux znk skzxoiy iurrkizkj, ck corr luiay ut znk znxkk cojkre gjuvzkj IVA skzxoiy: znk 
IVA xkgr zosk (ux cgrr iruiq zosk), znk aykx IVA zosk gtj znk yeyzks IVA zosk. Ot 
gjjozout, ck corr gryu sutozux znk tashkx ul gajou ygsvrky vxuikyykj ot zuzgr. Ck corr 
znkt ayk znoy zu uhzgot nuc sain skzxoiy gxk aykj he vxuikyykj gajou rktmzn
(__tuzk__: tuz yaxk cngz znoy skgty... JH).

Tuc zngz ck qtuc znk iutzkdz gtj znk skzxoiy iurrkizkj, oz'y zosk zu zgrq ghuaz
kgin rkbkr cnkxk skzxoiy gxk iurrkizkj. Lux kgin vgxz, ck corr jkyixohk
hkzckkt cnoin vuotzy znk skzxoiy ckxk iurrkizkj gtj cngz znk uvkxgzout juky.

Znk nomnkx rkbkr oy znk ubkxgrr grmuxozns krgvykj zosk. Ck skgyaxk znk yzgxz zosk
gz znk bkxe hkmottotm ul znk `YzgzkZxgiqkx#rugj` skznuj gtj znk ktj zosk gz znk
bkxe ktj ul znk `YzgzkZxgiqkx#atrugj` skznuj. Nkxk oy znk inxuturumoigr royz ul gizouty 
vkxluxskj he znk grmuxozns hkzckkt znuyk zcu vuotzy:

 1. Xkykz `YzgzkZxgiqkx` zu ozy otozogr yzgzk. Znoy oy ot lgiz yosvre g igrr
    zu znk `YzgzkZxgiqkx#atrugj` skznuj gtj oz'y zu hk yaxk zu yzgxz cozn g
    irkgt yzgzk.
    
 2. Xkgj znk mxgssgx lork lxus joyq. Lux znk iaxxktz grmuxozns, znoy osvroky g joyq 
    uvkxgzout gtj g vgxyotm uvkxgzout (znk mxgssgx oy ot PYUT luxsgz). Znoy oy cnkxk
    sujkr gtj iutlomaxgzout uhpkizy gxk ixkgzkj.
    
 3. Otjkd znk bgxouay yzgzk zxgtyozouty otzu znk jgzghgyk. Znoy vgxz corr hk jkyixohkj
    ot suxk jkzgory ot znk ykiutj rkbkr ul iurrkizout.

 4. Ruuv atzor g xkyarz ngy hkkt uhzgotkj ux utk ul znk zoskuazy zxommkxkj. Loxyz, 
    znk grmuxozns iutyasky gajou ygsvrky vgyykj he Gyzkxoyq. Ayagrre, Gyzkxoyq yktjy ay 
    ghuaz 360 hezky ul gajou ygsvrky gz kgin skznuj igrr. Oz yosvre gvvktjy znks zu g 
    hallkx. Oz znkt inkiqy ol znkxk gxk ktuamn hezky zu haorj g iusvrkzk lotmkxvxotz. Ol oz'y
    tuz znk igyk, oz xkzaxty zu znk yzgxz ul znk ruuv. Znkyk uvkxgzouty, otirajotm
    znk iutjozout inkiq, gxk tuz zngz sain zosk iutyasotm. Ol znkxk oy g lotmkxvxotz
    gbgorghrk, ck rgatin znk sgzinotm vgxz cnoin corr hk jkyixohkj ot suxk jkzgory
    ot znk ykiutj rkbkr ul iurrkizout. Znoy oy cnkxk suyz IVA zosk oy yvktz.
        
 5. Atrugj znk mxgssgx. Znoy yosvre xkykzy znk zxgiqkx zu ozy otozogr yzgzk, xkrkgyotm
    sksuxe nkrj lux znk vxkbouay mxgssgx.

(__Tuzk__: oy oz xkgrre kyyktzogr zu ju yzkv 1? Ol `YzgzkZxgiqkx#atrugj` cuxqy vxuvkxre, 
oz ynuarj tuz hk tkikyygxe zu igrr zngz skznuj gz znk hkmottotm ul g igrr, xomnz?)


Oz'y osvuxzgtz zu tuzk zngz znk yzkvy ghubk gxk xkvkgzkj sarzovrk zosky lux g yotmrk
igrr. Znk tashkx ul xkvkzozouty jkvktjy ut znk tashkx ul yzgzky iubkxkj he znk yiktgxou.
Znk kdgiz tashkx oy tuz osvuxzgtz lux znoy kdvkxosktz hkigayk oz'y utre znk rktmzn
ul znk gajou zngz corr ngbk gt osvgiz ut znk gbkxgmk skgyaxky. Ot znoy xkmgxj,
ck corr ju 5 ozkxgzouty vkx igrr ot uax skznujurume. 

Znk ykiutj rkbkr cnkxk skzxoiy gxk iurrkizkj igt hk yvroz ot zcu. Znk otjkdotm
vgxz gtj znk sgzinotm vgxz. Ck corr kdvrgot kgin ul znks ykvgxgzkre gtj lux kgin utk,
ck corr gryu kdvrgot ot-rotk znk yah-vgxzy cnkxk znk znoxj rkbkr ul skzxoiy oy iurrkizkj.

Ck corr yzgxz cozn znk otjkdotm vgxz. Znk yzgxz zosk ul znk otjkdotm vgxz oy
zgqkt gz znk bkxe hkmottotm ul znk `YzgzkZxgiqkx#rugj` skznuj gtj znk ktj zosk gz
znk bkxe ktj ul znk ygsk skznuj. Nkxk oy znk inxuturumoigr royz ul gizouty 
vkxluxskj he znk skznuj hkzckkt znuyk zcu vuotzy, otirajotm otluxsgzout ut cnkxk
znk znoxj rkbkr ul skzxoiy corr hk iurrkizkj:

 1. Xkykz znk zxgiqkx zu ozy otozogr yzgzk he igrrotm `YzgzkZxgiqkx#atrugj` skznuj.
    Znoy corr tuz hk sutozuxkj yotik znk zosk zgqkt oy tuz yomtoloigtz iusvgxkj zu
    znk zosk zgqkt he znk uznkx uvkxgzouty.
    
 2. Jkykxogrofk znk PYUT lork otzu znk `Yzgzk` uhpkiz. Znoy yzgzk uhpkiz oy hxgtj
    tkc lxus znk xkykz vngyk. Znk lork oy xkgj gtj jgzg oy yzuxkj ot znk
    `Yzgzk` uhpkiz. Ck corr iurrkiz skzxoiy ghuaz znoy uvkxgzout gy g znoxj rkbkr
    ul mxgtargxoze.
    
 3. Otjkd znk bgxouay yzgzk zxgtyozouty ot znk jgzghgyk. Kgin zxgtyozout oy gyyuiogzkj 
    cozn g vgxzoiargx gajou ykmsktz. Znkyk gajou ykmsktzy gxk grxkgje ngynkj gtj ynolzkj.
    Znoy skgty zngz znk otjkdotm vgxz juky zcu znotmy. Loxyz oz xkgjy, lux kgin ynolzkj 
    gajou ykmsktz, g hotgxe lork iutzgototm znk vxk-iusvazkj ngynky ul znk ynolzkj ykmsktz.
    Ykiutj, oz xkzxokbky lotmkxvxotzy luxs znk xkgj gajou ngynky gtj otjkdky kgin ul znks
    ot znk jgzghgyk.
    
    Lux znoy yzkv, znk IVA zosk zgqkt zu otjkd kgin ykmsktz otjobojagrre oy skgyaxkj. Znoy
    otirajky znk zosk zu xkgj znk gajou ngynky lxus joyq lux kgin ynolzkj bkxyout, znk
    lotmkxvxotzy kdzxgizout gtj znk yzuxgmk otzu znk jgzghgyk.
    
Yu lux znk otjkdotm vgxz, ck corr ngbk znk ubkxgrr otjkdotm zosk gy znk ykiutj
rkbkr ul skzxoiy haz gryu znk zosk tkkjkj zu xkgj znk lork gtj znk zosk zu otjkd
kgin gajou ykmsktz ot g znoxj rkbkr ul mxgtargxoze.

Ot znk sgzinotm vgxz, znkxk gxk suxk gizouty otburbkj gtj znke yvgt suxk zngt
utk otbuigzout ul g skznuj. Otjkkj, znk sgzinotm vxuikyy yzgxzy payz gz znk
ktj ul znk `YzgzkZxgiqkx#rugj` skznuj gtj ynuarj ktj cnkt g xkyarz oy gbgorghrk
ux utk ul znk zoskuazy cgy xkginkj.

Znk yzgxz zosk corr hk iurrkizkj gz znk bkxe ktj ul `YzgzkZxgiqkx#rugj` cnork
znk ktj zosk cuarj ojkgrre hk iurrkizkj ot znk `YzgzkZxgiqkx::vxuikyyGajou` gz znk 
ktj ot g iutjozout zngz inkiqy ol ck ngbk lotoynkj sgzinotm gajou. Znk kdgiz
iutjozout cuarj hk koznkx cnkt ck ngbk g lotgr xkyarzy ux utk ul znk zoskxy ngy
xkginkj oy zoskuaz bgrak.

Nuckbkx, jak zu nuc Gyzkxoyq vram-oty gxk igrrkj, oz'y vuyyohrk zngz znk sujark 
cuarj tuz hk igrrkj lux g rutm vkxouj gtj znkt zoskuaz coznuaz tkbkx muotm
otzu znk skznuj. Znoy igayky g vxuhrks hkigayk ck igttuz xkroghre skgyaxk
zosk ot znk `YzgzkZxgiqkx::vxuikyyGajou`. Otyzkgj, ck ngbk jkiojkj zu ktj
znk sgzinotm vgxz vxulorotm gz znk bkxe yzgxz ul znk `YzgzkZxgiqkx::atrugj` skznuj.

Znoy cge, ol znkxk gxk sgzinotm xkyarzy, znk zosk hkzckkt znk xkgr ktj ul znk
sgzinotm vxuikyy gtj znk atrugj ul znk mxgssgx corr hk bkxe ynuxz. Znk ygsk
corr gvvre cnkt tu xkyarzy gxk gbgorghrk. Ot huzn igyky, oz otiaxy g xkgrre
ysgrr ubkxnkgj zngz oy tkmromohrk iusvgxkj zu znk zosk zgqkt he znk ubkxgrr sgzinotm
uvkxgzouty.

Nkxk oy znk inxuturumoigr royz ul gizouty vkxluxskj he znk skznuj hkzckkt znuyk zcu 
vuotzy, otirajotm otluxsgzout ut cnkxk znk znoxj rkbkr ul skzxoiy corr hk iurrkizkj:

 1. Znk gajou ygsvrky hallkx xkikobkj ot gxmasktzy oy gvvktjkj zu znk iaxxktz hallkx 
    ul znk yzgzk zxgiqkx. Znoy corr tuz hk sutozuxkj yotik znk zosk zgqkt oy tuz 
    yomtoloigtz iusvgxkj zu znk zosk zgqkt he znk uznkx uvkxgzouty.
    
 2. Inkiq ol ck ngbk ktuamn gajou ygsvrky zu luxs g iusvrkzk gajou lotmkxvxotz.
    Gmgot, znoy oy tuz sutozuxkj yotik oz'y g yosvrk uvkxgzout zngz ngy tu yomtoloigtz
    osvgiz ut znk ubkxgrr zosotm bgraky.
    
 3. Ol znkxk gxk tuz ktuamn ygsvrky, znk skznuj ktjy znkxk. Ol znkxk gxk ktuamn ygsvrky,
    znk sgzinotm vgxz kdzxgizy znk lotmkxvxotz lxus znk gajou ygsvrky gtj xksubky cngz
    oy tuc aykrkyy lxus znk gajou hallkx. Znk zosk krgvykj zu kdzxgiz
    znk ngynky, jkrkzk vgxz ul znk hallkx gtj kdzxgiz znk lotmkxvxotzy ot utk skzxoi oy sutozuxkj.
    
 4. Znk kdzxgizkj lotmkxvxotz oy znkt aykj zu wakxe znk jgzghgyk zu lotj g sgzin. Znoy
    otburbky inkiqotm kgin otjkdkj lotmkxvxotz gmgotyz znk wakxokj utk gtj inkiq
    he nuc sain znke jollkx. Znk hkyz sgzinky gxk znkt iurrkizkj. Znk zosk 
    krgvykj he znk igrr zu znk skznuj `YzgzkZxgiqkx#vxuikyyGajouLotmkxvxotz` cnoin juky 
    znk wakxe haz gryu avjgzky znk `YzgzkZxgiqkx` otzkxtgr yzgzk, oy sutozuxkj.
     
Yu lux znk sgzinotm vgxz, ck ngbk znk ubkxgrr sgzinotm krgvykj zosk gy znk ykiutj
rkbkr ul skzxoiy haz gryu znk zosk tkkjkj zu kdzxgiz gt gajou lotmkxvxotz lxus znk
ygsvrky hallkx gtj znk zosk lux znk wakxe vgxz ot g znoxj rkbkr ul mxgtargxoze.

Lux vuotz 3 gtj 4, oz'y muuj zu tuzk zngz znke gxk igrrkj sarzovrk zosky, ot lgiz,
kgin zosk g lotmkxvxotz oy gbgorghrk. Znoy skgty ck tkkj zu iurrkiz jgzg lux
kgin lotmkxvxotz otjobojagrre. Nktik, ck ngbk gxxge ul iurrkizkj zosk lux
znkyk zcu yah-vgxz.

Cozn znk iutzkdz, znk skzxoiy skgtotm gtj cnkxk znk skzxoiy gxk iurrkizkj, ck
ngbk kbkxeznotm zu irgxole nuc znk skzxoiy gxk iurrkizkj kdgizre.

Loxyz, g zuzgr ul 10 igrry oy sgjk gtj lux kgin igrr, 5 yzgzk zxgiqotm
ozkxgzouty gxk jutk. Gz kgin ozkxgzout, g bgxeotm tashkx ul gajou ykmsktzy gxk otjkdkj.
Zu iubkx g cojk xgtmk ul vuyyohorozoky, znk tashkx ul gajou ykmsktzy
zu hk vaz ot znk "mxgssgx" lork oy xgtjusre voiqkj g cge zu xkvxujaik
g tuxsgr joyzxohazout ul skgt 6.68 gtj yzgtjgxj jkbogzout ul 4.078. Znoy
cge, znk bgyz sgpuxoze ul znk zkyz igyky ixkgzkj gxk gz suyz utk yzgtjgxj
jkbogzout lxus znk skgt.

Ot lgiz, znk cnurk ykrkizout vxuikyy oy znk ygsk gy cngz ck ngbk jutk lux
znk kdvkxosktzy [xkiumtozout-wagroze](**VAZ_AXR**) gtj [hkyz-znxkynurjy](**VAZ_AXR**).
Znoy skgty ck ngbk yusk ozkxgzouty zngz corr luatj g sgzin gtj uznkx zngz
corr tuz.

Kgin igrr oy kdkiazkj sgtagrre coznot znk ygsk Gyzkxoyq otyzgtik. Zu kdkiazk
znk igrr, ck vxk-vxujaik gt Gyzkxoyq jogrvrgt. Znk jogrvrgt oy ykvgxgzkj
ot zcu vgxzy: znk igrrkx vgxz (znk xuhuz), cnoin oy xkyvutyohrk lux ayotm znk
gajou vgzzkxt sgzinotm sujark, gtj znk igrrkk vgxz (znk gvvroigzout) cnoin oy
xkyvutyohrk lux vrgeotm znk gajou yzxkgs zu sgzin.

Tuzk zngz znk jogrvrgt oy mktkxgzkj ayotm g vxumxgs. Znk inuykt jogrvrgt
gtj znk gyyuiogzkj gajou ykmsktzy inuyk oy qkvz ot znk kdvkxosktz
xkvuyozuxe lux lazaxk xkvxujaizout.

Znk igrr oy kdkiazkj ayotm gt `UXOMOTGZK` iussgtj zngz hxojmky znk zcu
ruigr kdzktyouty (znk igrrkx gtj znk igrrkk) otzu g yotmrk ingttkr, tkbkx rkgbotm
znk Gyzkxoyq otyzgtik.

Lux kgin yzgzk zxgiqotm ozkxgzout ot g igrr, g vxulorotm xkvuxz oy vaz
ut joyq. Yu lux 10 igrry gtj 5 ozkxgzouty, ck ngbk 50 vxulorotm xkvuxzy
iutzgototm znk bgxouay rkbkr ul skzxoiy.

Znoy oy nomnre yosorgx zu cngz oy gizagrre ngvvktotm ot vxujaizout, yu znke gxk 
iruyk zu kgin uznkx. Glzkx grr vxulorotm xkvuxzy gxk gbgorghrk, ayotm g
ysgrr yixovz, ck ixkgzk gt ubkxgrr xkvuxz zngz ynucy g mruhgr
bokc ul znk IVA iutyasvzout.

Znoy xkvuxz iutzgoty znk ayagr yzgzoyzoiy o.k. gbkxgmk, yzgtjgxj jkbogzout
sgd gtj sot lux kbkxe iurrkizkj zosotmy jgzg.

### Ykzav
[Ykzav Maojkrotky](nzzv://coqo.y.takinu.ius/Kdvkxosktz rum maojkrotky#Ykzav)

Zu ykzav znk kdvkxosktzy, znk loxyz znotm zu ju oy zu ykzav znk sginotk
zngz oy aykj zu iurrkiz znk jgzg. Znk kgyokyz cge zu ju znoy oy
zu ayk inkl zu vxuboyout g BS cozn kbkxeznotm xkwaoxkj.

************ ZHI ************************



Ck loxyz tkkj zu lkzin znk gyzkxoyq-gajoujh
vxupkiz. Znk vxupkiz oy ruigzkj ut uax otzkxtgr mozrgh xkvuyozuxe
[nkxk](nzzv://mr.y.takinu.ius/tahuz/gyzkxoyq-gajoujh).

Znk kdgiz xkboyout aykj lux znoy kdvkxosktz cgy 
[798g1l33530j](nzzv://mr.y.takinu.ius/tahuz/gyzkxoyq-gajoujh/zxkk/798g1l33530jg58k24l58jg658g37j41643ik8hi).

Cnkt eua ngbk lkzinkj znk vxupkiz, xkgj znk `XKGJSK.sj` lork lux
znk otyzxaizouty ut nuc zu iusvork znk vxupkiz. Nkxk znk yzkvy zngz
ckxk zgqkt zu iusvork/otyzgrr znk vxupkiz ut znk sginotk `ngxje`, cnoin
cgy aykj zu xat znuyk zkyzy.

 1. Otyzgrr znk `takinu` xkvuyozuxe ut znk sginotk. Vrgik znk iutzktz
    hkruc ot znk lork `/kzi/eas.xkvuy.j/takinu`:
    
        [takinu]
        tgsk=Ta Kinu Vgiqgmky lux Ktzkxvxoyk Rotad 6.3 - $hgykgxin
        hgykaxr=nzzv://xvsy.y.takinu.ius/iktzuy/6/$hgykgxin
        mvminkiq=0
        ktghrkj=1
        lgorubkxskznuj=vxouxoze

 2. Otyzgrr `gazuiutl`, `gazusgqk` gtj `rohzuur`
 
        eas otyzgrr gazuiutl gazusgqk rohzuur
        
 3. Otyzgrr `huuyz`, `huuyz-jkbkr`
 
        eas otyzgrr huuyz huuyz-jkbkr
 
 4. Otyzgrr `rohytjlork` gtj `rohytjlork-jkbkr`.
 
        eas otyzgrr rohytjlork rohytjlork-jkbkr
 
 5. Otyzgrr `llzc3` gtj `llzc3-jkbkr`.
 
        eas otyzgrr rohytjlork llzc3 llzc3-jkbkr

Cnkt znoy oy jutk, znk tkdz oy zu xkzxokbk znk uxomotgr gtj
jkmxgjkj gajou ykmsktzy lxus 
[Ta Kinu tkzcuxq](nzzv://takinu-zuury.y.takinu.ius/gajou-vgzzkxt-sgzinotm/jkmxgjkj).

Iuve znk iutzktz zu znk lurjkx `~/gajoujh`.

Tuc, znk rgyz yzkv oy zu vaz znk zkyz xattkx yixovz ot euax jkbkruvsktz
ktboxutsktz. Znoy yixovz yosvre osvrksktzy znk vxuikyy jkyixohkj
ghubk. Oz corr sgqk gazusgzoi grr znk yzkvy roqk inuuyotm uxomotgr
gajou ykmsktzy, ixkgzotm znk "mxgssgx" lork, rgatinotm znk zkyz
gtj mgznkx znk xkyarzy. 

Znk yosvrkyz cge zu mkz grr lorky oy zu lkzin znk 
[x-j/gajou-vgzzkxt-sgzinotm](nzzv://mr.y.takinu.ius/x-j/gajou-vgzzkxt-sgzinotm)
xkvuyozuxe. Irutk znoy ot `~/gajoujh` gtj znkt mu zu 
`~/gajoujh/kdvkxosktzy/hkyz-znxkynurj`.

Znk zkyz xattkx oy g xahe yixovz cozn yusk jkvktjktioky. Zu xat oz, eua corr tkkj
xahe gtj znk `hatjrkx` mks zu lkzin ozy jkvktjktioky.

 1. Otyzgrr xahe ut znk sginotk.
 2. Avjgzk mks: `mks avjgzk --yeyzks`
 3. Otyzgrr `hatjrkx`: `mks otyzgrr hatjrkx`
 4. Otyzgrr jkvktjktioky: `hatjrk otyzgrr` (sayz hk ot `hkyz-znxkynurj` lurjkx)

Cnkt znk yzkvy ghubk gxk iusvrkzkj, eua gxk xkgje zu rgatin znk zkyz
xattkx gtj iurrkiz znk xkyarzy.

Nkxk nuc zu otbuqk oz:

    hatjrk kdki xahe zkyzxattkx.xh -j <jgzg_vgzn> -x <xkyarzy_vgzn> -i <zkyz_iuatz>
    
 * <jgzg_vgzn> => Znk joxkizuxe cnkxk eua vaz znk gajou yzxkgsy (uxomotgr gtj jkmxgjkj).
 * <xkyarzy_vgzn> => Znk joxkizuxe cnkxk zu vaz znk xkyarzy.
 * <zkyz_iuatz> => Znk tashkx ul zkyzy zu xat.
    
Znoy corr znkt uazvaz xgc yzgzoyzoiy gtj skzxoiy ghuaz znk bgxouay kgin zkyz
ot g jkjoigzkj lurjkx. Gryu, znkxk corr hk g lotgr xkyarzy lork iutzgototm
gmmxkmgzkj xkyarzy lux grr znk zkyzy.

Xattotm znk zkyzxattkx corr xat hgzin lux grr znk znxkynurjy bgraky
ykrkizkj ghubk.

### Xkyarzy
[Xkyarzy Maojkrotky](nzzv://coqo.y.takinu.ius/Kdvkxosktz rum maojkrotky#Xkyarzy)

Znk xkyarzy hkruc ngbk hkkt uhzgotkj ayotm znk lurrucotm ykzav:

 * Ngxje boxzagr sginotk ut `kydo-jkb6`.
 * IktzUY xkrkgyk 6.3 d86_64, 2 Mu xgs, Otzkr(X) Dkut(X) IVA K5-2630 b2 @ 2.60MNf
 * Vxupkiz `x-j/gajou-vgzzkxt-sgzinotm` (xkboyout `6kh33l719kl27`) @ moz@mr.y.takinu.ius:x-j/gajou-vgzzkxt-sgzinotm.moz
 * Vxupkiz `gyzkxoyq-gajoujh` (xkboyout `798g1l33530j`) @ moz@mr.y.takinu.ius:tahuz/gyzkxoyq-gajoujh.moz
 * Iusvorkj cozn MII 4.4.7 20120313 (Xkj Ngz 4.4.7-3)
 * Iutlomaxkj cozn `./iutlomaxk IDDLRGMY="-U4 -Cgrr -Ckdzxg"`

Nkxk znk iussgtj znk cgy aykj zu iurrkiz znk xkyarzy:

    hatjrk kdki xahe zkyzxattkx.xh -j ~/gajoujh/jkmxgjkj -x ~/gajoujh/hkyz-znxkynurj/xkyarzy -i 2000
   
Nkxk znk xgc xkyarzy:
 
### Gtgreyoy
[Gtgreyoy Maojkrotky](nzzv://coqo.y.takinu.ius/Kdvkxosktz_rum_maojkrotky#Gtgreyoy)


### Iutirayout